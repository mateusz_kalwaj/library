package pl.mk.library;

class Book {

    /**setting fields for user input(must be private to avoid unexpected changes made by user) */
    private String title;

    private String author;

    private  String year;


    /** setting place for storing user inputs */
    Book(String title, String author, String year){
        this.title = title;
        this.author = author;
        this.year = year;
    }
    /**declaration of "get" and "set" to be used for data flow with main class inquires */
    String getTitle() {

        return title;
    }


        String getAuthor() {

            return author;
    }
        String getYear() {

            return year;
    }
}
