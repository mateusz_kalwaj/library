package pl.mk.library;

/** Number of books can now be assigned to this shelf.
 *
 */
class Bookshelf {

    /** Declaration that new created Shelf would be a part of this Bookshelf class.
     *
     */
    private Shelf [] shelves = new Shelf[5];

    /** Add new shelf.
     *
     * @param index Index in array where shelf is.
     * @param shelf new shelf to be stored.
     */

    void setShelf (int index, Shelf shelf) {
        shelves [index] = shelf;
    }

    /** Gets previously stored shelf
     *
     * @param index Index of books to be returned.
     * @return Book from provided index.
     */
    Shelf getShelf (int index){
        return shelves[index];
    }
}
