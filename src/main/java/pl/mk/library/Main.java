package pl.mk.library;

import java.util.Scanner;

/** App main class */
public class Main {

    /** Library is now created to store all the bookshelves*/
    private static Library library =new Library();

    /** App starting method */
    public static void main(String[] args){

        /* Class Scanner has been added as user input */
        Scanner userInput = new Scanner(System.in);

        /* Declaring variable "boolean" which will be used to set loop as infinity */
        boolean programRun = true;

        /* Welcome screen output with first instruction */
        System.out.println("Welcome in books antique store! \n" +
                "First, press 1 and hit enter to add new book \n" +
                "After that, choose a place for your book (1-5) and enter book data such as title, author,year ");

        /* Infinite loop because of boolean variable included.   */
        while (programRun) {
            int command = userInput.nextInt();
            /*Automatic, user-dependant scenario selection (1,2 or 0) */
            switch (command) {
                case 1: {
                    /* Add new book using method */
                    addNewBook(userInput);
                    break;
                }
                case 2: {
                    /* Print new book/books using method */
                    printBook (userInput);
                    break;
                }
                case 0: {
                    /* Escape from switch only */
                    programRun = false;
                    break;
                }
                default: {
                    /* Reference to the new method*/
                    printManual();
                }
            }
        }
    }

    /** Reads user input and adds new book */
    private static void printBook(Scanner userInput) {

        System.out.println("Enter what book index you want to get");
        /* int index is necessary to read index for book to be printed */
        int index = userInput.nextInt();

        System.out.println("Enter what shelf index you want to get");
        /* int shelfIndex is necessary to read and then print index of shelves[] */
        int shelfIndex = userInput.nextInt();

        System.out.println("Enter what bookshelf index you want to get");
        /* int bookshelfIndex is necessary to read and then print bookshelf index given by user */
        int bookshelfIndex = userInput.nextInt();

        /* Reference changed to bookshelf. It is now reference to bookshelf object*/
        System.out.println("Title: " + library.getBookshelf(bookshelfIndex).getShelf(shelfIndex).getBook(index).getTitle());
        System.out.println("Author: " + library.getBookshelf(bookshelfIndex).getShelf(shelfIndex).getBook(index).getAuthor());
        System.out.println("Year: " + library.getBookshelf(bookshelfIndex).getShelf(shelfIndex).getBook(index).getYear());
    }
    /** Reads user input and prints selected book */
    private static void addNewBook(Scanner userInput) {

        /* int index is necessary to read index for new book*/
        System.out.println("Enter book index");
        int index = userInput.nextInt();

        /* New variable created (only in this block) to read index[] for new book*/
        System.out.println("Enter shelf index");
        int shelfIndex = userInput.nextInt();

        /* int bookshelfIndex is necessary to read index for new shelf*/
        System.out.println("Enter Bookshelf index");
        int bookshelfIndex = userInput.nextInt();

        /* Create local variables. They exist only in this {} */
        System.out.println("Enter book title");
        String title = userInput.next();
        System.out.println("Enter author");
        String author = userInput.next();
        System.out.println("Enter year of publish");
        String year = userInput.next();
        printManual();

        /* Create new book */
        Book book = new Book(title, author, year);

        /* If statement is necessary to check if shelf already exist. If not, it is creating one */
        if (library.getBookshelf(bookshelfIndex) == null){
            Bookshelf bookshelf = new Bookshelf();
            /* Local variables to more global assignment */
            library.setBookshelf(bookshelfIndex, bookshelf);
        }

        /* If statement is necessary to avoid overwriting particular shelves */
        if (library.getBookshelf(bookshelfIndex).getShelf(shelfIndex) == null) {
            /* If there's no shelf, create one */
            Shelf shelf = new Shelf();
            /* Now, once new bookshelf is created, program needs to assign it to the new create object bookshelf */
            library.getBookshelf(bookshelfIndex).setShelf(shelfIndex, shelf);
            shelf.setBook(index, book);
        } else {
            library.getBookshelf(bookshelfIndex).getShelf(shelfIndex).setBook(index, book);
        }
    }
    /**Prints manual */
    private static void printManual() {
        System.out.println("Valid commands available for you: \n" +
                "1 - add book to the collection \n" +
                "2 - show me my book \n" +
                "0 - get me out of here \n");
    }

}