package pl.mk.library;

/** Number of books can now be assigned to this shelf.N
 *
 */
class Shelf {

    /** Declaration that new created book would be a part of this shelf class.
     *
     */
    private Book[] books = new Book[5];


    /** Add new book.
     *
     * @param index Index in array where book is.
     * @param book new book to be stored.
     */
    void setBook(int index, Book book){
        books[index] = book;
    }

    /** Gets previously stored book
     *
     * @param index Index of books to be returned.
     * @return Book from provided index.
     */
    Book getBook(int index){

        return books[index];
    }
}


