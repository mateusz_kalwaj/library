package pl.mk.library;
/** Number of books can now be assigned to this shelf.
 *
 */
public class Library {

    /** Declaration that new created Shelf would be a part of this Bookshelf class.
     *
     */
    private Bookshelf[] bookshelves = new Bookshelf[5];

    /** Add new bookshelf.
     *
     * @param index Index in array where the bookshelf is.
     * @param bookshelf new bookshelf to be stored
     */

    public void setBookshelf (int index, Bookshelf bookshelf){
        bookshelves[index] = bookshelf;
    }

    /** Gets previously stored bookshelf
     *
     * @param bookshelfIndex Index of bookshelves to be returned.
     * @return bookshelf from index
     */

    public Bookshelf getBookshelf (int bookshelfIndex){
        return bookshelves[bookshelfIndex];
    }

}
